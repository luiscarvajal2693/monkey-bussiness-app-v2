//
//  PageItemController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 16/02/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation

class PageItemController: UIViewController {
    
    // MARK: - Variables
    var itemIndex: Int = 0
    var imageName: String = "" {
        
        didSet {
            
            if let imageView = contentImageView {
                imageView.image = UIImage(named: imageName)
            }
            
        }
    }
    var imageCenter: String = "" {
        
        didSet {
            
            if let imageView = contentImageCenter {
                imageView.image = UIImage(named: imageCenter)
            }
            
        }
    }
    
    var ReadText: String = "" {
        
        didSet {
            
            if let imageView = contentLabelText {
                imageView.text = ReadText
            }
            
        }
    }
    
    var audioText: String = "" {
        
        didSet {
            
            if let imageView = contentAudioText {
                imageView.text = audioText
            }
            
        }
    }
    
    var AppearText: String = "" {
        
        didSet {
            
            if let imageView = contentLabelText {
                imageView.text = AppearText
            }
            
        }
    }

    @IBOutlet var contentImageCenter: UIImageView?
    @IBOutlet var contentImageView: UIImageView?
    
    @IBOutlet var contentLabelText: UILabel!
    @IBOutlet var contentAudioText: UILabel!
    
    
    // MARK: - View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        
        
        contentImageView!.image = UIImage(named: imageName)
        self.contentImageCenter!.image = UIImage(named: self.imageCenter)
        
        if(imageCenter=="2-4A.png"){
            _ = Timer.scheduledTimer(timeInterval: 2.6, target: self, selector: #selector(PageItemController.changeImageKoala24A), userInfo: nil, repeats: false)
        }
        if(imageCenter=="2-5A.png"){
            _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(PageItemController.changeImageKoala25A), userInfo: nil, repeats: false)
        }
        if(imageCenter=="2-8A.png"){
            _ = Timer.scheduledTimer(timeInterval: 11.5, target: self, selector: #selector(PageItemController.changeImageKoala28A), userInfo: nil, repeats: false)
        }
        if(imageCenter=="5-6a.png"){
            _ = Timer.scheduledTimer(timeInterval: 7.0, target: self, selector: #selector(PageItemController.changeImagePolarBear56A), userInfo: nil, repeats: false)
        }
        
        //contentImageCenter!.image = UIImage(named: imageCenter)
        contentLabelText.text = ReadText
        contentAudioText.text = audioText
        contentAudioText.alpha = 0.0
        
        if(contentAudioText.text != ""){
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: contentAudioText.text, ofType: "mp3")!)
        
            do {
                // Removed deprecated use of AVAudioSessionDelegate protocol
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                try AVAudioSession.sharedInstance().setActive(true)
                
            } catch _ {
            }
            do {
                try AVAudioSession.sharedInstance().setActive(true)
            } catch _ {
            }
        
            //var error:NSError?
        
            do {
                historyPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                historyPlayer = AVAudioPlayer()
            }
            historyPlayer.prepareToPlay()
            if(type_of_read == "autoplay"){
                historyPlayer.play()
            }
        }
    }
    
    @IBAction func replaySoundText(_ sender: UIButton) {
        if(historyPlayer.isPlaying == false){
            historyPlayer.play()
        }
        else{
            historyPlayer.currentTime = 0
            historyPlayer.play()
        }
    }
    
    
    @objc func changeImageKoala24A(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-4A.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(PageItemController.changeImageKoala24B), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala24B(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-4B.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(PageItemController.changeImageKoala24C), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala24C(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-4C.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(PageItemController.changeImageKoala24A), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala25A(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-5A.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PageItemController.changeImageKoala25B), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala25B(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-5B.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PageItemController.changeImageKoala25C), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala25C(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-5C.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PageItemController.changeImageKoala25Astandby), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala25Astandby(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-5A.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(PageItemController.changeImageKoala25A1), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala25A1(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-5A.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PageItemController.changeImageKoala25B1), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala25B1(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-5B.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PageItemController.changeImageKoala25C1), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala25C1(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-5C.png")
            self.contentImageCenter!.alpha = 1
        })
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PageItemController.changeImageKoala25Astandby), userInfo: nil, repeats: false)
    }
    
    @objc func changeImageKoala28A(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "2-8B.png")
            self.contentImageCenter!.alpha = 1
        })
    }
    
    @objc func changeImagePolarBear56A(){
        UIView.animate(withDuration: 1.0, animations: {
            self.contentImageCenter!.image = UIImage(named: "5-6a2.png")
            self.contentImageCenter!.alpha = 1
        })
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
