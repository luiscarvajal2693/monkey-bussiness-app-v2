//
//  MondayRoutineViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 20/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class MondayRoutineViewController: UIViewController {
    
    @IBOutlet weak var btn_card1: UIButton!
    @IBOutlet weak var card1: UIImageView!
    @IBOutlet weak var card2: UIImageView!
    @IBOutlet weak var card3: UIImageView!
    @IBOutlet weak var card4: UIImageView!
    @IBOutlet weak var card5: UIImageView!
    @IBOutlet weak var card6: UIImageView!
    @IBOutlet weak var card7: UIImageView!
    @IBOutlet weak var card8: UIImageView!
    @IBOutlet weak var card9: UIImageView!
    
    @IBOutlet weak var btn_completed: UIButton!
    @IBOutlet weak var label_error: UILabel!
    
    @IBOutlet weak var btn_card2: UIButton!
    @IBOutlet weak var b_card1: UIImageView!
    @IBOutlet weak var b_card2: UIImageView!
    @IBOutlet weak var b_card3: UIImageView!
    @IBOutlet weak var b_card4: UIImageView!
    @IBOutlet weak var b_card5: UIImageView!
    @IBOutlet weak var b_card6: UIImageView!
    @IBOutlet weak var b_card7: UIImageView!
    @IBOutlet weak var b_card8: UIImageView!
    @IBOutlet weak var b_card9: UIImageView!
    
    @IBOutlet weak var btn_card3: UIButton!
    @IBOutlet weak var c_card1: UIImageView!
    @IBOutlet weak var c_card2: UIImageView!
    @IBOutlet weak var c_card3: UIImageView!
    @IBOutlet weak var c_card4: UIImageView!
    @IBOutlet weak var c_card5: UIImageView!
    @IBOutlet weak var c_card6: UIImageView!
    @IBOutlet weak var c_card7: UIImageView!
    @IBOutlet weak var c_card8: UIImageView!
    @IBOutlet weak var c_card9: UIImageView!
    @IBOutlet weak var btn_info: UIButton!
    
    @IBOutlet weak var cheetah1: UIImageView!
    @IBOutlet weak var cheetah2: UIImageView!
    @IBOutlet weak var cheetah3: UIImageView!
    
    @IBOutlet weak var animStar: UIImageView!
    
    var card1_actual: String!
    var actual1: Int!
    var card2_actual: String!
    var actual2: Int!
    var card3_actual: String!
    var actual3: Int!
    var star : UIImageView!
    var timer : Timer!
    
    @IBOutlet weak var viewProvisional: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        card1_actual = ""
        card2_actual = ""
        card3_actual = ""
        actual1 = 0
        actual2 = 0
        actual3 = 0
        activity_name = "activityC"
        pausa = false
        
        self.card1.alpha = 0.0
        self.card2.alpha = 0.0
        self.card3.alpha = 0.0
        self.card4.alpha = 0.0
        self.card5.alpha = 0.0
        self.card6.alpha = 0.0
        self.card7.alpha = 0.0
        self.card8.alpha = 0.0
        self.card9.alpha = 0.0

        self.b_card1.alpha = 0.0
        self.b_card2.alpha = 0.0
        self.b_card3.alpha = 0.0
        self.b_card4.alpha = 0.0
        self.b_card5.alpha = 0.0
        self.b_card6.alpha = 0.0
        self.b_card7.alpha = 0.0
        self.b_card8.alpha = 0.0
        self.b_card9.alpha = 0.0
        
        self.c_card1.alpha = 0.0
        self.c_card2.alpha = 0.0
        self.c_card3.alpha = 0.0
        self.c_card4.alpha = 0.0
        self.c_card5.alpha = 0.0
        self.c_card6.alpha = 0.0
        self.c_card7.alpha = 0.0
        self.c_card8.alpha = 0.0
        self.c_card9.alpha = 0.0
        
        self.cheetah1.alpha = 0.0
        self.cheetah2.alpha = 0.0
        self.cheetah3.alpha = 0.0
        
        accionMenu = ""
        do {
            // Removed deprecated use of AVAudioSessionDelegate protocol
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch _ {
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch _ {
        }
        
        //Creamos un timer para llamar a la animación de cargar
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MondayRoutineViewController.loadIntro), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionInfo(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MondayRoutine")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func loadIntro(){
        self.cheetah1.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
        UIView.animate(withDuration: 2.0, delay: 1.0, options: .curveEaseOut, animations: {
            self.cheetah1.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
            self.cheetah1.alpha = 1.0
            }, completion: nil)
        
        self.cheetah2.frame = CGRect(x: 392, y: 85, width: 256, height: 450)
        UIView.animate(withDuration: 2.0, delay: 3.0, options: .curveEaseOut, animations: {
            self.cheetah2.frame = CGRect(x: 392, y: 268, width: 256, height: 450)
            self.cheetah2.alpha = 1.0
            }, completion: nil)
        
        self.cheetah3.frame = CGRect(x: 712, y: 85, width: 256, height: 450)
        UIView.animate(withDuration: 2.0, delay: 5.0, options: .curveEaseOut, animations: {
            self.cheetah3.frame = CGRect(x: 712, y: 268, width: 256, height: 450)
            self.cheetah3.alpha = 1.0
            }, completion: nil)
        
        UIView.animate(withDuration: 8.0, delay: 0.0, options: .curveEaseOut, animations: {
            let sonido_activity = "Cheetah-says-her-routine"
            
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            
            }, completion: { finished in self.delay()
        })
    }
    
    func delay(){
        if(pausa == false){
            timer = Timer.scheduledTimer(timeInterval: 9.5, target: self, selector: #selector(MondayRoutineViewController.loadElements), userInfo: nil, repeats: false)
        }
    }
    
    @objc func loadElements(){
        self.cheetah1.alpha = 0.0
        self.cheetah2.alpha = 0.0
        self.cheetah3.alpha = 0.0
        
        UIView.animate(withDuration: 3.0, delay: 0.0, options: .curveEaseOut, animations: {
            let sonido_activity = "monkey-wants-to-know"
            
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            
            }, completion: { finished in self.delayAnswer()
        })
    }
    
    func delayAnswer(){
        if(pausa == false){
            _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(MondayRoutineViewController.loadAnswer), userInfo: nil, repeats: false)
        }
    }
    
    @objc func loadAnswer(){
        let sonido_activity = "Touch-the-red-arrows"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        UIView.animate(withDuration: 1.0, delay: 4.5, options: .curveEaseOut, animations: {
            self.btn_card1.alpha = 1.0
            
            }, completion: { finished in self.delayArrow2()
        })
    }
    
    func delayArrow2(){
        UIView.animate(withDuration: 1.0, delay: 1.5, options: .curveEaseOut, animations: {
            self.btn_card2.alpha = 1.0
            
            }, completion: { finished in self.delayArrow3()
        })
    }

    func delayArrow3(){
        UIView.animate(withDuration: 1.0, delay: 0.6, options: .curveEaseOut, animations: {
            self.btn_card3.alpha = 1.0
            
            }, completion: { finished in self.delayStar1()
        })
    }
    
    func delayStar1(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.animStar.alpha = 1.0
            self.animStar.frame = CGRect(x: 467, y: 26, width: 103, height: 103)
            }, completion: { finished in self.delayStar2()
        })
    }
    
    func delayStar2(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.animStar.alpha = 1.0
            self.animStar.frame = CGRect(x: 474, y: 36, width: 89, height: 89)
            }, completion: { finished in self.activeBtnStar()
        })
    }
    
    func activeBtnStar(){
        self.animStar.alpha = 0.0
        self.btn_completed.alpha = 1.0
    }
    
    func AudioCard1(){
        let sonido_activity = "art"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard2(){
        let sonido_activity = "Music"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard3(){
        let sonido_activity = "playground"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard4(){
        let sonido_activity = "reading"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard5(){
        let sonido_activity = "ride-a-bike"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard6(){
        let sonido_activity = "school"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard7(){
        let sonido_activity = "sleep"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard8(){
        let sonido_activity = "sport"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    func AudioCard9(){
        let sonido_activity = "TV"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    @IBAction func ActionCard1(_ sender: AnyObject){
        actual1 = actual1 + 1
        if(actual1 == 0){
            self.card1.alpha = 0.0
            self.card2.alpha = 0.0
            self.card3.alpha = 0.0
            self.card4.alpha = 0.0
            self.card5.alpha = 0.0
            self.card6.alpha = 0.0
            self.card7.alpha = 0.0
            self.card8.alpha = 0.0
            self.card9.alpha = 0.0
            card1_actual = ""
        }
        else if(actual1 == 1){
            self.card1.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card1.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card1.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card9.alpha = 0.0
                self.card1.alpha = 1.0
            })
            card1_actual = "card1"
            AudioCard6()
        }
        else if(actual1 == 2){
            self.card2.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card2.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card2.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card1.alpha = 0.0
                self.card2.alpha = 1.0
            })
            card1_actual = "card2"
            AudioCard2()
        }
        else if(actual1 == 3){
            self.card3.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card3.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card3.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card2.alpha = 0.0
                self.card3.alpha = 1.0
            })
            card1_actual = "card3"
            AudioCard3()
        }
        else if(actual1 == 4){
            self.card4.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card4.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card4.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card3.alpha = 0.0
                self.card4.alpha = 1.0
            })
            card1_actual = "card4"
            AudioCard4()
        }
        else if(actual1 == 5){
            self.card5.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card5.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card5.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card4.alpha = 0.0
                self.card5.alpha = 1.0
            })
            card1_actual = "card5"
            AudioCard5()
        }
        else if(actual1 == 6){
            self.card6.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card6.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card6.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card5.alpha = 0.0
                self.card6.alpha = 1.0
            })
            card1_actual = "card6"
            AudioCard1()
        }
        else if(actual1 == 7){
            self.card7.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card7.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card7.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card6.alpha = 0.0
                self.card7.alpha = 1.0
            })
            card1_actual = "card7"
            AudioCard7()
        }
        else if(actual1 == 8){
            self.card8.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card8.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card8.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card7.alpha = 0.0
                self.card8.alpha = 1.0
            })
            card1_actual = "card8"
            AudioCard8()
        }
        else if(actual1 == 9){
            self.card9.frame = CGRect(x: 50, y: 85, width: 256, height: 450)
            self.card9.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
                self.card9.frame = CGRect(x: 50, y: 268, width: 256, height: 450)
                self.card8.alpha = 0.0
                self.card9.alpha = 1.0
            })
            card1_actual = "card9"
            actual1 = 0
            AudioCard9()
        }
    }
    
    @IBAction func ActionCard2(_ sender: AnyObject){
        actual2 = actual2 + 1
        if(actual2 == 0){
            self.b_card1.alpha = 0.0
            self.b_card2.alpha = 0.0
            self.b_card3.alpha = 0.0
            self.b_card4.alpha = 0.0
            self.b_card5.alpha = 0.0
            self.b_card6.alpha = 0.0
            self.b_card7.alpha = 0.0
            self.b_card8.alpha = 0.0
            self.b_card9.alpha = 0.0
            card2_actual = ""
        }
        else if(actual2 == 1){
            self.b_card1.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card1.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card9.alpha = 0.0
                self.b_card1.alpha = 1.0
            })
            card2_actual = "card1"
            AudioCard1()
        }
        else if(actual2 == 2){
            self.b_card2.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card2.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card1.alpha = 0.0
                self.b_card2.alpha = 1.0
            })
            card2_actual = "card2"
            AudioCard2()
        }
        else if(actual2 == 3){
            self.b_card3.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card3.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card2.alpha = 0.0
                self.b_card3.alpha = 1.0
            })
            card2_actual = "card3"
            AudioCard3()
        }
        else if(actual2 == 4){
            self.b_card4.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card4.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card3.alpha = 0.0
                self.b_card4.alpha = 1.0
            })
            card2_actual = "card4"
            AudioCard4()
        }
        else if(actual2 == 5){
            self.b_card5.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card5.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card4.alpha = 0.0
                self.b_card5.alpha = 1.0
            })
            card2_actual = "card5"
            AudioCard5()
        }
        else if(actual2 == 6){
            self.b_card6.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card6.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card5.alpha = 0.0
                self.b_card6.alpha = 1.0
            })
            card2_actual = "card6"
            AudioCard6()
        }
        else if(actual2 == 7){
            self.b_card7.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card7.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card6.alpha = 0.0
                self.b_card7.alpha = 1.0
            })
            card2_actual = "card7"
            AudioCard7()
        }
        else if(actual2 == 8){
            self.b_card8.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card8.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card7.alpha = 0.0
                self.b_card8.alpha = 1.0
            })
            card2_actual = "card8"
            AudioCard8()
        }
        else if(actual2 == 9){
            self.b_card9.frame = CGRect(x: 385, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.b_card9.frame = CGRect(x: 385, y: 268, width: 256, height: 450)
                self.b_card8.alpha = 0.0
                self.b_card9.alpha = 1.0
            })
            card2_actual = "card9"
            AudioCard9()
            actual2 = 0
        }
    }
    
    @IBAction func ActionCard3(_ sender: AnyObject){
        actual3 = actual3 + 1
        if(actual3 == 0){
            self.c_card1.alpha = 0.0
            self.c_card2.alpha = 0.0
            self.c_card3.alpha = 0.0
            self.c_card4.alpha = 0.0
            self.c_card5.alpha = 0.0
            self.c_card6.alpha = 0.0
            self.c_card7.alpha = 0.0
            self.c_card8.alpha = 0.0
            self.c_card9.alpha = 0.0
            card3_actual = ""
        }
        else if(actual3 == 1){
            self.c_card1.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card1.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card9.alpha = 0.0
                self.c_card1.alpha = 1.0
            })
            card3_actual = "card1"
            AudioCard7()
        }
        else if(actual3 == 2){
            self.c_card2.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card2.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card1.alpha = 0.0
                self.c_card2.alpha = 1.0
            })
            card3_actual = "card2"
            AudioCard2()
        }
        else if(actual3 == 3){
            self.c_card3.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card3.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card2.alpha = 0.0
                self.c_card3.alpha = 1.0
            })
            card3_actual = "card3"
            AudioCard3()
        }
        else if(actual3 == 4){
            self.c_card4.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card4.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card3.alpha = 0.0
                self.c_card4.alpha = 1.0
            })
            card3_actual = "card4"
            AudioCard4()
        }
        else if(actual3 == 5){
            self.c_card5.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card5.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card4.alpha = 0.0
                self.c_card5.alpha = 1.0
            })
            card3_actual = "card5"
            AudioCard5()
        }
        else if(actual3 == 6){
            self.c_card6.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card6.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card5.alpha = 0.0
                self.c_card6.alpha = 1.0
            })
            card3_actual = "card6"
            AudioCard6()
        }
        else if(actual3 == 7){
            self.c_card7.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card7.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card6.alpha = 0.0
                self.c_card7.alpha = 1.0
            })
            card3_actual = "card7"
            AudioCard1()
        }
        else if(actual3 == 8){
            self.c_card8.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card8.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card7.alpha = 0.0
                self.c_card8.alpha = 1.0
            })
            card3_actual = "card8"
            AudioCard8()
        }
        else if(actual3 == 9){
            self.c_card9.frame = CGRect(x: 710, y: 85, width: 256, height: 450)
            UIView.animate(withDuration: 1.0, animations: {
                self.c_card9.frame = CGRect(x: 710, y: 268, width: 256, height: 450)
                self.c_card8.alpha = 0.0
                self.c_card9.alpha = 1.0
            })
            card3_actual = "card9"
            AudioCard9()
            actual3 = 0
        }
    }

    @IBAction func ActionCompleted(_ sender: UIButton) {
        if(card1_actual=="" || card2_actual=="" || card3_actual==""){
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MondayRoutineViewController.badAnswer), userInfo: nil, repeats: false)
        }
        else{
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MondayRoutineViewController.rightAnswer), userInfo: nil, repeats: false)
        }
    }
    
    @objc func rightAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "Great"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        //Función animación Estrellita
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MondayRoutineViewController.animationStar), userInfo: nil, repeats: false)
    }
    
    @objc func badAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "please-try-again"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    @IBAction func ActionBack(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MenuOpenView")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func animationStar(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "thanks-you-have-completed-this-activity"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        /*star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 300, y: 764, width: 100, height: 100)
        star.alpha = 0.7
        view.addSubview(star)
        UIView.animateWithDuration(2.0, animations: {
        self.star.frame = CGRect(x: 300+60, y: 764-204, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(4.0, animations: {
        self.star.frame = CGRect(x: 360+10, y: 560-440, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(6.0, animations: {
        self.star.frame = CGRect(x: 600+10, y: 120-70, width: 84, height: 78)
        self.star.alpha = 1.0
        })*/
        
        star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 200, y: 800, width: 100, height: 100)
        self.view.addSubview(star)
        
        // randomly create a value between 0.0 and 150.0
        _ = CGFloat( arc4random_uniform(150))
        
        // for every y-value on the bezier curve
        // add our random y offset so that each individual animation
        // will appear at a different y-position
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 200,y: 800))
        path.addCurve(to: CGPoint(x: 810, y: -130), controlPoint1: CGPoint(x: 536, y: 473), controlPoint2: CGPoint(x: 778, y: 40))
        
        // create the animation
        let anim = CAKeyframeAnimation(keyPath: "position")
        anim.path = path.cgPath
        anim.rotationMode = CAAnimationRotationMode.rotateAuto
        anim.repeatCount = 1
        anim.duration = 2.0
        
        // add the animation
        star.layer.add(anim, forKey: "animate position along path")
        
        let filePath = Bundle.main.path(forResource: "sfx_star", ofType: "mp3")
        let fileURL = URL(fileURLWithPath: filePath!)
        var soundID:SystemSoundID = 0
        AudioServicesCreateSystemSoundID(fileURL as CFURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(MondayRoutineViewController.setAccionMenu), userInfo: nil, repeats: false)
        
    }
    
    @objc func setAccionMenu(){
        star.alpha = 0.0
        Userstars = Userstars + 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
