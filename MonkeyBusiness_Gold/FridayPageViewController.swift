//
//  FridayPageViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 26/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import Foundation

class FridayPageViewController: UIViewController {
    
    @IBOutlet var btn_replay: UIButton!
    @IBOutlet var btn_off_replay: UIButton!
    @IBOutlet var btn_change_chapter: UIButton!
    @IBOutlet var containerMenu: UIView!
    @IBOutlet var containerPager: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        day = "Friday"
        ultimo = false
        
        if(type_of_read == "autoplay"){
            historyPlayer.play()
            btn_replay.alpha = 0.0
            btn_off_replay.alpha = 1.0
        }
        else{
            btn_replay.alpha = 1.0
            btn_off_replay.alpha = 0.0
        }
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(FridayPageViewController.handleSwipes(_:)))
        leftSwipe.direction = .left
        self.btn_change_chapter.addGestureRecognizer(leftSwipe)

        // Do any additional setup after loading the view, typically from a nib.
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(FridayPageViewController.evaluateEndPage), userInfo: nil, repeats: true)
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            print("Swipe Left")
            if(historyPlayer.isPlaying == true){
                historyPlayer.stop()
            }
            day = "Saturday"
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                self.containerPager.frame.origin.x = -1024
                self.btn_change_chapter.frame.origin.x = -1024
                }, completion: { finished in self.changeChapter()
            })
        }
    }
    
    func changeChapter(){
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
        self.present(fvc!, animated: false, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func replaySoundText(_ sender: UIButton) {
        self.btn_off_replay.alpha = 1.0
        self.btn_replay.alpha = 0.0
        if(historyPlayer.isPlaying == false){
            historyPlayer.play()
        }
        else{
            historyPlayer.currentTime = 0
            historyPlayer.play()
        }
    }
    
    @IBAction func OffReplaySoundText(_ sender: UIButton) {
        self.btn_replay.alpha = 1.0
        self.btn_off_replay.alpha = 0.0
        if(historyPlayer.isPlaying == true){
            historyPlayer.stop()
        }
    }
    
    @objc func evaluateEndPage(){
        if(ultimo == true){
            btn_change_chapter.alpha = 1.0
        }
        else if(ultimo == false){
            btn_change_chapter.alpha = 0.0
        }
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}
