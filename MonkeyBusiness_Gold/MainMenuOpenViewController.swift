//
//  MainMenuOpenViewController.swift
//  MonkeyBusiness_Gold
//
//  Created by Neymar Contreras on 25/02/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class MainMenuOpenViewController: UIViewController {
    @IBOutlet var menu: UIImageView!
    
    @IBOutlet var bg_scroll: UIImageView!
    @IBOutlet var text_read: UIImageView!
    @IBOutlet var text_auto: UIImageView!
    
    @IBOutlet weak var btn_home: UIButton!
    @IBOutlet weak var btn_home_On: UIButton!
    
    @IBOutlet var btn_close_menu: UIButton!
    @IBOutlet weak var btn_close_On: UIButton!
    
    @IBOutlet var btn_activityA: UIButton!
    @IBOutlet var btn_activityB: UIButton!
    @IBOutlet var btn_activityC: UIButton!
    @IBOutlet var btn_activityD: UIButton!
    @IBOutlet var btn_activityE: UIButton!
    @IBOutlet var aro_activityA: UIImageView!
    @IBOutlet var aro_activityB: UIImageView!
    @IBOutlet var aro_activityC: UIImageView!
    @IBOutlet var aro_activityD: UIImageView!
    @IBOutlet var aro_activityE: UIImageView!
    
    //@IBOutlet weak var btn_Sunday: UIButton!
    let btn_day = UIButton(type: UIButton.ButtonType.custom)
    @IBOutlet weak var btn_Sunday: UIButton!
    @IBOutlet weak var btn_Sunday_On: UIButton!
    @IBOutlet weak var btn_Monday: UIButton!
    @IBOutlet weak var btn_Monday_On: UIButton!
    @IBOutlet weak var btn_Tuesday: UIButton!
    @IBOutlet weak var btn_Tuesday_On: UIButton!
    @IBOutlet weak var btn_Wednesday: UIButton!
    @IBOutlet weak var btn_Wednesday_On: UIButton!
    @IBOutlet weak var btn_Thursday: UIButton!
    @IBOutlet weak var btn_Thursday_On: UIButton!
    @IBOutlet weak var btn_Friday: UIButton!
    @IBOutlet weak var btn_Friday_On: UIButton!
    @IBOutlet weak var btn_Saturday: UIButton!
    @IBOutlet weak var btn_Saturday_On: UIButton!
    
    @IBOutlet weak var btn_Home: UIButton!
    
    @IBOutlet weak var btn_boston_zoo: UIButton!
    @IBOutlet weak var btn_story_map: UIButton!
    @IBOutlet weak var btn_activity_map: UIButton!
    
    @IBOutlet var perfilView: UIImageView!
    @IBOutlet var perfilName: UILabel!
    @IBOutlet var profileView: UIView!
    @IBOutlet var perfilStars: UILabel!
    
    var ir : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ir = ""
        
        perfilView.image = imagePerfil
        perfilName.text = namePerfil as? String
        
        perfilView.layer.cornerRadius = perfilView.frame.size.width / 2
        perfilView.clipsToBounds = true
        perfilStars.text = String(Userstars)
        
        //Creamos un timer para llamar a la funcion de los labels tope menu
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MainMenuOpenViewController.revisionLabels), userInfo: nil, repeats: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func revisionLabels(){
        _ = self.menu.frame
        if(day == "Sunday"){
            btn_Sunday_On.alpha = 1.0
            btn_Sunday.alpha = 0.0
        }
        if(day == "Monday"){
            btn_Monday_On.alpha = 1.0
            btn_Monday.alpha = 0.0
        }
        if(day == "Tuesday"){
            btn_Tuesday_On.alpha = 1.0
            btn_Tuesday.alpha = 0.0
        }
        if(day == "Wednesday"){
            btn_Wednesday_On.alpha = 1.0
            btn_Wednesday.alpha = 0.0
        }
        if(day == "Thursday"){
            btn_Thursday_On.alpha = 1.0
            btn_Thursday.alpha = 0.0
        }
        if(day == "Friday"){
            btn_Friday_On.alpha = 1.0
            btn_Friday.alpha = 0.0
        }
        if(day == "Saturday"){
            btn_Saturday_On.alpha = 1.0
            btn_Saturday.alpha = 0.0
        }
        if(day == "activityA"){
            aro_activityA.alpha = 1.0
        }
        if(day == "activityB"){
            aro_activityB.alpha = 1.0
        }
        if(day == "activityC"){
            aro_activityC.alpha = 1.0
        }
        if(day == "activityD"){
            aro_activityD.alpha = 1.0
        }
        if(day == "activityE"){
            aro_activityE.alpha = 1.0
        }
    }
    
    
    @objc func GoHome() {
        if(ir == "OpenView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "OpenView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "DaysOfTheWeekView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "DaysOfTheWeekView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "FirstActivityView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "FirstActivityView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "MondayRoutine"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MondayRoutine")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "HowManyDays"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "HowManyDays")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "BeforeAndAfter"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "BeforeAndAfter")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "IntroDay"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "SundayView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "MondayView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "TuesdayView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "WednesdayView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "ThursdayView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "FridayView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "SaturdayView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
    }
    
    @IBAction func musicSliderValueChanged(_ sender: UISlider) {
        let sliderValue = sender.value
        soundPlayer.volume = sliderValue
    }
    
    func loadIntroView(){
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    func resetButtonsOff(){
        self.btn_Sunday.alpha = 1.0
        self.btn_Monday.alpha = 1.0
        self.btn_Tuesday.alpha = 1.0
        self.btn_Wednesday.alpha = 1.0
        self.btn_Thursday.alpha = 1.0
        self.btn_Friday.alpha = 1.0
        self.btn_Saturday.alpha = 1.0
        self.btn_home_On.alpha = 0.0
        self.btn_close_On.alpha = 0.0
        self.btn_Sunday_On.alpha = 0.0
        self.btn_Monday_On.alpha = 0.0
        self.btn_Tuesday_On.alpha = 0.0
        self.btn_Wednesday_On.alpha = 0.0
        self.btn_Thursday_On.alpha = 0.0
        self.btn_Friday_On.alpha = 0.0
        self.btn_Saturday_On.alpha = 0.0
        self.btn_home_On.alpha = 0.0
        self.btn_close_On.alpha = 0.0
        self.aro_activityA.alpha = 0.0
        self.aro_activityB.alpha = 0.0
        self.aro_activityC.alpha = 0.0
        self.aro_activityD.alpha = 0.0
        self.aro_activityE.alpha = 0.0
    }
    
    @IBAction func buttonHome(_ sender: AnyObject) {
        ir = "OpenView"
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let audio_btn = "home"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: audio_btn, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            soundPlayer.play()
            
            self.btn_home_On.alpha = 1.0
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonSunday(_ sender:UIButton!)
    {
        day = "Sunday"
        ir = "SundayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Giraffe"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Sunday_On.alpha = 1.0
            self.btn_Sunday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonMonday(_ sender:UIButton!)
    {
        day = "Monday"
        ir = "MondayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Koala-Bear"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Monday_On.alpha = 1.0
            self.btn_Monday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonTuesday(_ sender:UIButton!)
    {
        day = "Tuesday"
        ir = "TuesdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Cheetah"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                // error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Tuesday_On.alpha = 1.0
            self.btn_Tuesday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonWednesday(_ sender:UIButton!)
    {
        day = "Wednesday"
        ir = "WednesdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Crocodile"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //let error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Wednesday_On.alpha = 1.0
            self.btn_Wednesday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonThursday(_ sender:UIButton!)
    {
        day = "Thursday"
        ir = "ThursdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Polar-Bear"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Thursday_On.alpha = 1.0
            self.btn_Thursday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonFriday(_ sender:UIButton!)
    {
        day = "Friday"
        ir = "FridayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Elephant"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Friday_On.alpha = 1.0
            self.btn_Friday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonSaturday(_ sender:UIButton!)
    {
        day = "Saturday"
        ir = "SaturdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Monkey-Louie"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Saturday_On.alpha = 1.0
            self.btn_Saturday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    //Boton que hace el llamado al Menú Principal
    @IBAction func buttonOpenMenu(_ sender:UIButton!)
    {
       accionMenu = "Abrir Menu"
       if(activityPlayer.isPlaying == true){
            activityPlayer.stop()
       }
    }
    
    //Boton que hace el llamado a Cerrar el Menú Principal
    @IBAction func buttonCloseMenu(_ sender:UIButton!)
    {
        accionMenu = "Cerrar Menu"
        let sonido_activity = "Close"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        pausa = false
        
        if(activity_name == "activityA"){
            day = "activityA"
            ir = "DaysOfTheWeekView"
        }
        else if(activity_name == "activityB"){
            day = "activityB"
            ir = "FirstActivityView"
        }
        else if(activity_name == "activityC"){
            day = "activityC"
            ir = "MondayRoutine"
        }
        else if(activity_name == "activityD"){
            day = "activityD"
            ir = "HowManyDays"
        }
        else if(activity_name == "activityE"){
            day = "activityE"
            ir = "BeforeAndAfter"
        }
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MainMenuOpenViewController.closeMenuHome), userInfo: nil, repeats: false)
    }

    func closeMenu(){
        ir = "IntroDay"
        accionMenu = "Cerrar Menu"
        
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MainMenuOpenViewController.GoHome), userInfo: nil, repeats: false)
    }
    
   
    @objc func closeMenuHome(){
        GoHome()
    }

    @IBAction func buttonActivityA(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Counting-Days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityA"
        ir = "DaysOfTheWeekView"
        
        aro_activityA.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuOpenViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityB(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Earth-Rotation"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityB"
        ir = "FirstActivityView"
        
        aro_activityB.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuOpenViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityC(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Daily-Routine"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityC"
        ir = "MondayRoutine"
        
        aro_activityC.alpha = 1.0
        _  = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuOpenViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityD(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "How-Many-Times"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityD"
        ir = "HowManyDays"
        
        aro_activityD.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuOpenViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityE(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Before-and-After"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityE"
        ir = "BeforeAndAfter"
        
        aro_activityE.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuOpenViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionBostonZoo(_ sender: AnyObject) {
        let sonido_activity = "Boston_Zoo"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        
        soundPlayer.play()
    }
    
    
    @IBAction func ActionStoryMap(_ sender: AnyObject) {
        let sonido_activity = "Story-Map"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        
        soundPlayer.play()
    }
    
    
    @IBAction func ActionActivityMap(_ sender: AnyObject) {
        let sonido_activity = "ActivityMap"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        
        soundPlayer.play()
    }
    
    deinit {
        debugPrint("Menu deinitialized...")
    }
    
}
