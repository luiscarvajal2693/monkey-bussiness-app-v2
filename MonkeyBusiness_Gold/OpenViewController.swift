//
//  ViewController.swift
//  MonkeyBusiness_Gold
//
//  Created by Neymar Contreras on 17/02/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import CoreData

var accionMenu: String!

class OpenViewController: UIViewController{
    @IBOutlet var background: UIImageView!
    //@IBOutlet var settings: UIView!
    @IBOutlet var book: UIImageView!
    @IBOutlet var book_bg: UIImageView!
    
    var audioPlayer = AVAudioPlayer()
   // let button_autoread_Open = UIButton(type: UIButton.ButtonType.custom)
   // let button_readmyself_Open = UIButton(type: UIButton.ButtonType.custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        day = "Sunday"
        accionMenu = ""
        //---------- Se  habilita  por defecto el narrador auto
            type_of_read = "autoplay"
            let audio_btn = "A-for-Autoplay"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: audio_btn, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                self.audioPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                self.audioPlayer = AVAudioPlayer()
            }
            self.audioPlayer.prepareToPlay()
            
            self.audioPlayer.play()
        // ---------
        
  //      let autoread = UIImage(named: "btn_autoplay_menu_off.png") as UIImage?
  //      let autoread_on = UIImage(named: "btn_autoplay_menu_on.png") as UIImage?
        
  //      button_autoread_Open.frame = CGRect(x: 120, y: 650, width: 63, height: 57)
        
 //       button_autoread_Open.setImage(autoread, for: UIControl.State())
 //       button_autoread_Open.setImage(autoread_on, for: UIControl.State.highlighted)
 //       button_autoread_Open.setImage(autoread_on, for: UIControl.State.selected)
       // button_autoread_Open.addTarget(self, action: #selector(OpenViewController.buttonAutoPlay(_:)), for:.touchUpInside)
        
    //    self.view.addSubview(button_autoread_Open)
        
        let autoplay = UIImage(named: "btn_readmyself_menu_off.png") as UIImage?
        let autoplay_on = UIImage(named: "btn_readmyself_menu_on.png") as UIImage?
        
     //   button_readmyself_Open.frame = CGRect(x: 190, y: 650, width: 63, height: 57)
    //    button_readmyself_Open.setImage(autoplay, for: UIControl.State())
     //   button_readmyself_Open.setImage(autoplay_on, for: UIControl.State.highlighted)
    //    button_readmyself_Open.setImage(autoplay_on, for: UIControl.State.selected)
       // button_readmyself_Open.addTarget(self, action: #selector(OpenViewController.buttonReadMyself(_:)), for:.touchUpInside)
        
    //    button_readmyself_Open.isSelected = true
        
   //     self.view.addSubview(button_readmyself_Open)
        // var error:NSError?
    }
    
    /*
    
    @IBAction func buttonReadMyself(_ sender:UIButton!)
    {
        type_of_read = "read_myself"
        button_readmyself_Open.isSelected = true
        button_autoread_Open.isSelected = false
       
        let audio_btn = "R-for-Read-Myself"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: audio_btn, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            self.audioPlayer = AVAudioPlayer()
        }
        self.audioPlayer.prepareToPlay()
        
        self.audioPlayer.play()
    }
    
    @IBAction func buttonAutoPlay(_ sender:UIButton!)
    {
        type_of_read = "autoplay"
        button_autoread_Open.isSelected = true
        button_readmyself_Open.isSelected = false
        
        let audio_btn = "A-for-Autoplay"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: audio_btn, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            self.audioPlayer = AVAudioPlayer()
        }
        self.audioPlayer.prepareToPlay()
        
        self.audioPlayer.play()
    }
    */
    
    //Funcion de la animación del Libro al presionar el botón
    @IBAction func openBook(_ sender: AnyObject) {
        let filePath = Bundle.main.path(forResource: "Bubble Click", ofType: "mp3")
        let fileURL = URL(fileURLWithPath: filePath!)
        var soundID:SystemSoundID = 0
        AudioServicesCreateSystemSoundID(fileURL as CFURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        
        let childController = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
        addChild(childController!)
        self.view.addSubview((childController?.view)!)
        childController?.didMove(toParent: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}

