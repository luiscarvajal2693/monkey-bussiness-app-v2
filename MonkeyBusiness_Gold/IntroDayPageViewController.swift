//
//  IntroDayPageViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 01/04/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation


class IntroDayPageViewController: UIViewController {
    @IBOutlet weak var containerMenu: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        _ = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(IntroDayPageViewController.animationDay), userInfo: nil, repeats: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToSunday" {
            _ = segue.destination as! MainSundayViewController
        }
    }
    
    @objc func animationDay(){
        print(day)
        if(day == "Sunday"){
            /*let childController = self.storyboard?.instantiateViewControllerWithIdentifier("SundayView") as! UIViewController!
            addChildViewController(childController)
            self.view.addSubview(childController.view)
            childController.didMoveToParentViewController(self)
            */
            //self.performSegueWithIdentifier("segueToSunday", sender: self)
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "SundayView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(day == "Monday"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MondayView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(day == "Tuesday"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "TuesdayView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(day == "Wednesday"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "WednesdayView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(day == "Thursday"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "ThursdayView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(day == "Friday"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "FridayView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(day == "Saturday"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "SaturdayView")
            self.present(fvc!, animated: false, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}
