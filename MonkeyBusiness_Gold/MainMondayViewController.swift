
//
//  MainMondayViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 24/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//


import UIKit
import AVFoundation
import MediaPlayer

class MainMondayViewController: UIViewController, UIPageViewControllerDataSource{
    //weak var delegate : PlayerDelegate?
    
    // MARK: - Variables
    fileprivate var pageViewController: UIPageViewController?
    
    // Initialize it scrollimages
    fileprivate let contentImages = ["",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""];
    
    // Initialize it right away here
    fileprivate let centerImages = ["2-1A.png",
        "2-1B.png",
        "2-2.png",
        "2-3A.png",
        "2-3B.png",
        "2-3C.png",
        "2-4A.png",
        "2-5A.png",
        "2-6.png",
        "2-7.png",
        "2-8A.png"];
    
    
    // Initialize Labels Text
    fileprivate let textLabels = ["“Hello Mr. Koala, hello!...hello? Monkey Louie asks— are you sleeping?! ",
        "Mr. Koala suddenly wakes up and replies, “No, No, I’m awake” —“Riiiight”, says Monkey Louie— “Can you tell me what day did your food disappear?” “Yes, it was last Monday,” Koala replies “I remember that day I took a short nap, then when I woke up, my food had disappeared”",
        "Monkey Louie says “How do you know it was Monday? how do you know the days of the week? —“good question” says Koala, “Let me show you something”",
        "“Now let’s pretend this tennis ball is the planet Earth, we’re here in Boston’s Franklin Zoo and as you may know, I’m originally from Australia,",
        "so my home country is on the other side of the planet,",
        "now can you point the flash light towards this tennis ball?, let’s pretend your flash light is the Sun.",
        "“Look Monkey, the planet Earth rotates like this, when Boston is on the bright side it means it’s day time- at the same time- look on the other side of the planet, it’s dark, which means, it’s night time on the other side, so, when it’s day time in America it is night time in Australia.",
        "So when the planet does one full rotation, like this, it is the same as one full day. Now let’s pretend this full rotation is Sunday, so the next full rotation would be Monday, the next one Tuesday, then Wednesday, Thursday, Friday, Saturday, and then the week starts again.",
        "“Ok, Ok, Koala, that’s enough” —says Monkey Louie— “thanks, I know all about the planet earth rotation and the days of the week, I just wanted to make sure you knew, now please, let’s focus on my investigation. Do you remember anything unusual from that day?",
        "Koala replies: “Yes!, I remember there was a very bad- smell all around my cage that day, actually same smell as right now, can you smell it monkey?”.",
        "“Yes”—Says Monkey Louie “Maybe you need to take a shower Koala” — Koala replies; “That’s not funny Monkey, maybe you, maybe...{and koala fell asleep again} ”"];
    
    // Initialize it with sounds
    fileprivate let soundText = ["2.1A",
        "2.1B",
        "2.2",
        "2.3A1",
        "2.3A2",
        "2.3B",
        "2.4",
        "2.5",
        "2.6",
        "2.7",
        "2.8"];
    
    var timer = Timer()
    
    // MARK: - View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        createPageViewController()
        setupPageControl()
        audiotermino = false
        contador = 0
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MainMondayViewController.AudioFinished), userInfo: nil, repeats: true)
    }
    
    fileprivate func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChild(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
    }
    
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex > 0 {
            ultimo = false
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        else if itemController.itemIndex+1 == contentImages.count{
            ultimo = true
        }
        
        return nil
    }

    
    fileprivate func getItemController(_ itemIndex: Int) -> PageItemController? {
        
        if itemIndex < contentImages.count {
            
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! PageItemController
            
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.imageCenter = centerImages[itemIndex]
            pageItemController.ReadText = textLabels[itemIndex]
            pageItemController.audioText = soundText[itemIndex]
            pageItemController.AppearText = textLabels[itemIndex]
            
            actual_index = itemIndex
            
            return pageItemController
        }
        
        return nil
    }
    
    @objc func AudioFinished(){
        if(historyPlayer.isPlaying == false){
            contador += 1
        }
        if(contador>=6 && type_of_read == "autoplay" && standby == false){
            contador = 0
            if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![0])!) {
                
                pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                
                _ = self.children.last
            }
            if(ultimo==true){
                day = "Tuesday"
                let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
                self.present(fvc!, animated: false, completion: nil)
                timer.invalidate()
            }
        }
        
    }
    
    @IBAction func ActionNext(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    @IBAction func ActionPrev(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
        }
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

    
}
