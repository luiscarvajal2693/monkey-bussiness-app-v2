//
//  MainMenuBigViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 03/05/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//


import UIKit
import AVFoundation
import MediaPlayer

class MainMenuBigViewController: UIViewController {
    @IBOutlet var btn_activityA: UIButton!
    @IBOutlet var btn_activityB: UIButton!
    @IBOutlet var btn_activityC: UIButton!
    @IBOutlet var btn_activityD: UIButton!
    @IBOutlet var btn_activityE: UIButton!
    
    @IBOutlet weak var btn_Sunday: UIButton!
    @IBOutlet weak var btn_Monday: UIButton!
    @IBOutlet weak var btn_Tuesday: UIButton!
    @IBOutlet weak var btn_Wednesday: UIButton!
    @IBOutlet weak var btn_Thursday: UIButton!
    @IBOutlet weak var btn_Friday: UIButton!
    @IBOutlet weak var btn_Saturday: UIButton!
    
    @IBOutlet weak var btn_boston_zoo: UIButton!
    @IBOutlet weak var btn_story_map: UIButton!
    @IBOutlet weak var btn_activity_map: UIButton!
    
    @IBOutlet var dayImage: UIImageView!
    @IBOutlet var flechaImage: UIImageView!
    @IBOutlet var viewFlechaJiraffe : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        if(day == "Sunday"){
            dayImage.image = UIImage(named: "bg_zooMap_Sunday.jpg")
           _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MainMenuBigViewController.soundSunday), userInfo: nil, repeats: false)
        }
        if(day == "Monday"){
            dayImage.image = UIImage(named: "bg_zooMap_Monday.jpg")
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MainMenuBigViewController.soundMonday), userInfo: nil, repeats: false)
        }
        if(day == "Tuesday"){
            dayImage.image = UIImage(named: "bg_zooMap_Tuesday.jpg")
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MainMenuBigViewController.soundTuesday), userInfo: nil, repeats: false)
        }
        if(day == "Wednesday"){
            dayImage.image = UIImage(named: "bg_zooMap_Wednesday.jpg")
            _ = Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(MainMenuBigViewController.soundWednesday), userInfo: nil, repeats: false)
        }
        if(day == "Thursday"){
            dayImage.image = UIImage(named: "bg_zooMap_Thursday.jpg")
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MainMenuBigViewController.soundThursday), userInfo: nil, repeats: false)
        }
        if(day == "Friday"){
            dayImage.image = UIImage(named: "bg_zooMap_Friday.jpg")
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MainMenuBigViewController.soundFriday), userInfo: nil, repeats: false)
        }
        if(day == "Saturday"){
            dayImage.image = UIImage(named: "bg_zooMap_Saturday.jpg")
            _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(MainMenuBigViewController.soundSaturday), userInfo: nil, repeats: false)
        }
        
        
    }
    
    @objc func soundSunday(){
        let sonido_activity = "Sunday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuBigViewController.soundJiraffe), userInfo: nil, repeats: false)
    }
    
    @objc func soundJiraffe(){
        let sonido_activity = "Giraffe"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    @objc func soundMonday(){
        let sonido_activity = "Monday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuBigViewController.soundKoala), userInfo: nil, repeats: false)
    }
    
    @objc func soundKoala(){
        let sonido_activity = "Koala-Bear"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    @objc func soundTuesday(){
        let sonido_activity = "Tuesday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuBigViewController.soundCheetah), userInfo: nil, repeats: false)
    }
    
    @objc func soundCheetah(){
        let sonido_activity = "Cheetah"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    @objc func soundWednesday(){
        let sonido_activity = "Wednesday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(MainMenuBigViewController.soundCrocodile), userInfo: nil, repeats: false)
    }
    
    @objc func soundCrocodile(){
        let sonido_activity = "Crocodile"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    @objc func soundThursday(){
        let sonido_activity = "Thursday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuBigViewController.soundPolarBear), userInfo: nil, repeats: false)
    }
    
    @objc func soundPolarBear(){
        let sonido_activity = "Polar-Bear"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    @objc func soundFriday(){
        let sonido_activity = "Friday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuBigViewController.soundElephant), userInfo: nil, repeats: false)
    }
    
    @objc func soundElephant(){
        let sonido_activity = "Elephant"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    @objc func soundSaturday(){
        let sonido_activity = "Saturday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuBigViewController.soundMonkeyLouie), userInfo: nil, repeats: false)
    }
    
    @objc func soundMonkeyLouie(){
        let sonido_activity = "Monkey-Louie"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch  {
            // error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("Menu deinitialized...")
    }
    
}
