//
//  SundayPageViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 16/02/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//
import UIKit
import Foundation

var standby: Bool!

class SundayPageViewController: UIViewController {
    
    @IBOutlet var btn_replay: UIButton!
    @IBOutlet var btn_off_replay: UIButton!
    @IBOutlet var btn_change_chapter: UIButton!
    @IBOutlet var containerMenu: UIView!
    @IBOutlet var containerPager: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.removeFromParent()
        day = "Sunday"
        ultimo = false
        pausa = false
        standby = false
    
        if(type_of_read == "autoplay"){
            btn_replay.alpha = 0.0
            btn_off_replay.alpha = 1.0
        }
        else{
            btn_replay.alpha = 1.0
            btn_off_replay.alpha = 0.0
        }
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(SundayPageViewController.handleSwipes(_:)))
        leftSwipe.direction = .left
        self.btn_change_chapter.addGestureRecognizer(leftSwipe)

        // Do any additional setup after loading the view, typically from a nib.
         _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(SundayPageViewController.evaluateEndPage), userInfo: nil, repeats: true)
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            print("Swipe Left")
            if(historyPlayer.isPlaying == true){
                historyPlayer.stop()
            }
            day = "Monday"
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                self.containerPager.frame.origin.x = -1024
                self.btn_change_chapter.frame.origin.x = -1024
                }, completion: { finished in self.changeChapter()
            })
        }
    }
    
    func changeChapter(){
        day = "Monday"
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @IBAction func nextChapter(){
        day = "Monday"
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionDemo(_ sender: UIButton) {
        print("actionDemo")
    }
    
    @IBAction func replaySoundText(_ sender: UIButton) {
        self.btn_off_replay.alpha = 1.0
        self.btn_replay.alpha = 0.0
        if(historyPlayer.isPlaying == false){
            historyPlayer.play()
        }
        else{
            historyPlayer.currentTime = 0
            historyPlayer.play()
        }
    }
    
    @IBAction func OffReplaySoundText(_ sender: UIButton) {
        self.btn_replay.alpha = 1.0
        self.btn_off_replay.alpha = 0.0
        if(historyPlayer.isPlaying == true){
            historyPlayer.stop()
        }
    }
    
    @objc func evaluateEndPage(){
        if(ultimo == true){
            btn_change_chapter.alpha = 1.0
        }
        else if(ultimo == false){
            btn_change_chapter.alpha = 0.0
        }
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }
}
