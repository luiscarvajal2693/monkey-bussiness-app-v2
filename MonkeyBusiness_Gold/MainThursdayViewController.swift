//
//  MainThursdayViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 26/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class MainThursdayViewController: UIViewController, UIPageViewControllerDataSource{
    //weak var delegate : PlayerDelegate?
    
    // MARK: - Variables
    fileprivate var pageViewController: UIPageViewController?
    
    // Initialize it scrollimages
    fileprivate let contentImages = ["",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""];
    
    // Initialize it right away here
    fileprivate let centerImages = ["5-1a.png",
        "5-1b.png",
        "5-2a.png",
        "5-2b.png",
        "5-3a.png",
        "5-3b.png",
        "5-4a.png",
        "5-4b.png",
        "5-5.png",
        "5-6a.png",
        "5-6b.png"];
    
    
    // Initialize Labels Text
    fileprivate let textLabels = ["“Hello Polar”— says Monkey Louie— “so, what day did your food disappear?”",
        "—“last Thursday” Polar Bear replies—“I came back from a nice swim and I found that my fish and berries were gone”",
        "“Ok”—says Monkey— “What were you doing before your food disappeared?",
        "- “I just told you”, Polar Bear replies- “I was swimming in my frozen pool” —",
        "“Understood”—Monkey Louie says, “so, my next question is, what were your doing previous to the time your food disappeared?",
        "—“Are you kidding me Monkey?” says Polar Bear.",
        "“Alright, alright” replies Monkey—“Don’t get mad Polar, I know these are tough questions”",
        "—Polar replies “tough questions?, they sound more like silly questions, you know, before and previous is the same Monkey.”",
        "Monkey scratches his head while looking at his notes, then Polar Bear says, “stop your silly questions Monkey, do you want to know something strange that happened that day? After, my food was gone I found very small paw prints everywhere”",
        "Monkey Louie replies: “Maybe those were your own paw prints, everybody says you have baby paws Polar”",
        "— Polar Bear looks at his paws and replies: “alright that’s not funny Monkey, maybe you want to take a dive in my frozen pool?”"];
    
    // Initialize it with sounds
    fileprivate let soundText = ["5.1A",
        "5.1B",
        "5.2A",
        "5.2B",
        "5.3A",
        "5.3B",
        "5.4A",
        "5.4B",
        "5.5",
        "5.6A",
        "5.6B"];
    
    var timer = Timer()
    
    // MARK: - View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        createPageViewController()
        setupPageControl()
        audiotermino = false
        contador = 0
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MainThursdayViewController.AudioFinished), userInfo: nil, repeats: true)
    }
    
    fileprivate func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChild(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
    }
    
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.backgroundColor = UIColor.clear
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex > 0 {
            ultimo = false
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        else if itemController.itemIndex+1 == contentImages.count{
            ultimo = true
        }
        
        return nil
    }
    
    fileprivate func getItemController(_ itemIndex: Int) -> PageItemController? {
        
        if itemIndex < contentImages.count {
            
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! PageItemController
            
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.imageCenter = centerImages[itemIndex]
            pageItemController.ReadText = textLabels[itemIndex]
            pageItemController.audioText = soundText[itemIndex]
            pageItemController.AppearText = textLabels[itemIndex]
            
            actual_index = itemIndex
            
            return pageItemController
        }
        
        return nil
    }
    
    @objc func AudioFinished(){
        if(historyPlayer.isPlaying == false){
            contador += 1
        }
        if(contador>=6 && type_of_read == "autoplay" && standby == false){
            contador = 0
            if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![0])!) {
                
                pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                
                _ = self.children.last
            }
            if(ultimo==true){
                day = "Friday"
                let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
                self.present(fvc!, animated: false, completion: nil)
                timer.invalidate()
            }
        }
        
    }
    
    @IBAction func ActionNext(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    @IBAction func ActionPrev(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
        }
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }
    
}
